mod mouse_button_helper;
mod single_digit;

use enigo::*;
use mouse_button_helper::MouseButtonHelper;
#[cfg(any(unix))]
use nix::ifaddrs::InterfaceAddress;
use single_digit::SingleDigit;
use std::io::{Error, Read, Write};
#[allow(unused_imports)]
use std::net::{SocketAddr, TcpListener, TcpStream, UdpSocket};
use std::str::{self, FromStr};
use std::thread;
use std::time::Duration;

// pattern: [magic number = 0][text = buffer[1..bytes_read]]
fn use_mode_keyboard(enigo: &mut Enigo, buffer: &[u8], bytes_read: usize) -> Result<(), String> {
    println!("Using keyboard mode");
    let string = str::from_utf8(&buffer[1..bytes_read]).unwrap_or_default();
    println!("Keyboard data received: {}", string);
    for letter in string.chars() {
        enigo.key_click(Key::Layout(letter))
    }
    Ok(())
}

// pattern: [magic number = 1][mouse_behaviour = 0-3][mouse_button = 0-2 or mouse_relative = {x, y}]
fn use_mode_mouse(enigo: &mut Enigo, buffer: &[u8], bytes_read: usize) -> Result<(), String> {
    println!("Using mouse mode");
    return match SingleDigit::new_from_ascii(buffer[1]) {
        Ok(mouse_behaviour) => {
            println!("Mouse behaviour: {}", mouse_behaviour.value());
            return match mouse_behaviour.value() {
                // mouse click / down / up
                0..=2 => match SingleDigit::new_from_ascii(buffer[2]) {
                    Ok(mouse_button_id) => {
                        println!("Mouse button: {}", mouse_button_id.value());
                        match MouseButtonHelper::button_from_digit(mouse_button_id) {
                            Ok(mouse_button) => match mouse_behaviour.value() {
                                0 => Ok(enigo.mouse_click(mouse_button)),
                                1 => Ok(enigo.mouse_down(mouse_button)),
                                2 => Ok(enigo.mouse_up(mouse_button)),
                                _ => panic!("Impossible case"),
                            },
                            Err(error) => Err(format!("{}, got: {}", error, buffer[2])),
                        }
                    }
                    Err(error) => Err(format!("{}, got: {}", error, buffer[2])),
                },
                // mouse move
                3 => {
                    if bytes_read >= 4 {
                        let x: i32 = buffer[2] as i32;
                        let y: i32 = buffer[3] as i32;
                        println!("Moving mouse by: {} {}", x, y);
                        Ok(enigo.mouse_move_relative(x, y))
                    } else {
                        Err(format!(
                            "Invalid data for relative mouse move: {}:{}",
                            buffer[3], buffer[4]
                        ))
                    }
                }
                _ => Err(format!("Unknown mouse mode: {}", mouse_behaviour.value())),
            };
        }
        Err(error) => Err(format!("{}, got: {}", error, buffer[1])),
    };
}

fn handle_request(mut stream: TcpStream) -> Result<(), Error> {
    let mut enigo: Enigo = Enigo::new();
    let mut buffer: [u8; 512] = [0 as u8; 512]; // 512 byte buffer

    loop {
        let bytes_read: usize = stream.read(&mut buffer)?;
        // magic number + data
        if bytes_read < 2 {
            return Ok({});
        }
        let magic_number = SingleDigit::new_from_ascii(buffer[0]);
        let response: Result<(), String> = match magic_number {
            Ok(magic_number) => {
                println!("Magic number: {:?}", magic_number.value());
                match magic_number.value() {
                    0 => use_mode_keyboard(&mut enigo, &buffer[..256], bytes_read),
                    1 => use_mode_mouse(&mut enigo, &buffer[..4], bytes_read),
                    _ => Err(
                        stringify!("Buffer does not conform to [magic_number][data]").to_string(),
                    ),
                }
            }
            Err(error) => Err(format!("{}, got: {}", error, buffer[0])),
        };
        match response {
            Ok(_) => stream.write(&buffer[..bytes_read]),
            Err(error) => {
                eprintln!("{}", error);
                stream.write(format!("{}\n", error).as_bytes())
            }
        }?;
    }
}
#[cfg(any(unix))]
fn find_broadcastable_interface() -> Result<InterfaceAddress, ()> {
    let addrs = nix::ifaddrs::getifaddrs().unwrap();
    for ifaddr in addrs {
        if ifaddr.address.is_some() && ifaddr.netmask.is_some() && ifaddr.broadcast.is_some() {
            return Ok(ifaddr);
        }
    }
    return Err({});
}

fn main() {
    let listen_port: i32 = 8080;
    let listener: TcpListener = TcpListener::bind(format!("0.0.0.0:{}", listen_port))
        .expect(concat!("Unable to bind TCP port ", stringify!(port)));
    println!("Listening on port {}", listen_port);
    thread::spawn(move || {
        let broadcast_remote_port: i32 = 9999;
        let broadcaster: UdpSocket = UdpSocket::bind("0.0.0.0:0").expect("Unable to bind UDP port");
        broadcaster
            .set_read_timeout(Some(Duration::from_secs(5)))
            .unwrap();
        broadcaster
            .set_broadcast(true)
            .expect("Unable to set UPD to broadcast");
        println!("Broadcasting to port {}", broadcast_remote_port);
        #[allow(unused_mut)]
        let mut broadcast_target: String = format!("192.168.1.255:{}", broadcast_remote_port.to_string());
        #[cfg(any(unix))]
        match find_broadcastable_interface() {
            Ok(ifaddr) => match SocketAddr::from_str(&ifaddr.broadcast.unwrap().to_string()) {
                Ok(mut broadcast_address) => {
                    broadcast_address.set_port(broadcast_remote_port as u16);
                    broadcast_target = broadcast_address.to_string();
                }
                Err(error) => panic!("{}", error),
            },
            Err(_) => panic!("Unable to determine an interface with broadcast abilities"),
        }
        let message: &mut[u8; 8] = &mut[1, 2, 3, 5, 8, 13, 0, 0];
        if listen_port > u8::MAX.into() {
            let listen_port_string: String = listen_port.to_string();
            message[6] = u8::from_str(&listen_port_string[0..2]).unwrap();
            message[7] = u8::from_str(&listen_port_string[2..4]).unwrap();
            
        }
        else {
            message[7] = listen_port as u8;
        }
        loop {
            match broadcaster.send_to(message, &broadcast_target) {
                Err(error) => {
                    eprintln!("UDP error: {:#?}", error);
                    break;
                }
                Ok(_) => (),
            }
            thread::sleep(Duration::from_secs(3));
        }
        drop(broadcaster);
    });
    for possible_stream in listener.incoming() {
        match possible_stream {
            Ok(stream) => {
                println!("Incoming connection: {}", stream.peer_addr().unwrap());
                thread::spawn(move || {
                    handle_request(stream).unwrap_or_else(|error| eprintln!("{:?}", error));
                });
            }
            Err(e) => {
                eprintln!("TCP error: {:#?}", e);
            }
        }
    }
    drop(listener);
}
