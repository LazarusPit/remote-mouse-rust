use std::fmt;

pub struct SingleDigit {
    value: u8
}

#[derive(Debug, Clone)]
pub struct NotSingleDigitError {}

impl fmt::Display for NotSingleDigitError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "Ascii value must be between 48 and 58 to be a digit")
    }
 }

impl SingleDigit {
    pub fn new_from_ascii(value: u8) -> Result<SingleDigit, NotSingleDigitError> {
        return if value < 48 || value > 58 {
            Err(NotSingleDigitError {})
        }
        else {
            Ok(SingleDigit { value: value - 48 })
        }
    }

    pub fn value(&self) -> u8 {
        self.value
    }
}