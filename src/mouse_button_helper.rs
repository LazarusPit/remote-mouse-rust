use crate::single_digit::SingleDigit;

use std::fmt;
use enigo::MouseButton;

#[derive(Debug, Clone)]
pub struct UnknownMouseButtonError {}

impl fmt::Display for UnknownMouseButtonError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "Mouse button id must be between 0-4")
    }
 }

pub struct MouseButtonHelper {}

impl MouseButtonHelper {
    pub fn button_from_digit(digit: SingleDigit) -> Result<MouseButton, UnknownMouseButtonError> {
        match digit.value() {
            0 => Ok(MouseButton::Left),
            1 => Ok(MouseButton::Middle),
            2 => Ok(MouseButton::Right),
            3 => Ok(MouseButton::ScrollDown),
            4 => Ok(MouseButton::ScrollUp),
            _ => Err(UnknownMouseButtonError {})
        }
    }
}